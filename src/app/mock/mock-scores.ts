import { Score } from '../models/score';

import { Student } from '../models/student';
import { STUDENTS } from '../mock/mock-students';

const students: Student[] = STUDENTS;

export const SCORES: Score[] = [
    {Student: students[0], score: 0, full_mark: 100},
    {Student: students[1], score: 1, full_mark: 100},
    {Student: students[2], score: 30, full_mark: 100},
    {Student: students[3], score: 42, full_mark: 100},
    {Student: students[4], score: 51, full_mark: 100},

];
