import { Lecturer } from '../models/lecturer';

export const LECTURERS: Lecturer[] = [
    {id: 521, name: 'irelia'},
    {id: 500, name: 'twisted fate'},
    {id: 1180, name: 'Yuumi'},
    {id: 860, name: 'ahri'},
    {id: 300, name: 'riven'},
];
