import { Student } from '../models/student';

export const STUDENTS: Student[] = [
    {id: 60111111, name: 'Yasuo', email: 'Yasuo_k0d555a0@gmail.com'},
    {id: 60111112, name: 'Yone', email: 'YoneNameofBrothercancer@gmail.com'},
    {id: 60111113, name: 'Malphite', email: 'Therock@gmail.com'},
    {id: 60111114, name: 'Teemo', email: 'CaptainTeemo@gmail.com'},
    {id: 60111115, name: 'Soraka', email: 'SorakaTank@gmail.com'},
];
